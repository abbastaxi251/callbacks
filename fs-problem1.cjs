const fs = require("fs")

const deleteFiles = (directoryPath, numberOfFiles, callback) => {
    let count = 0
    for (let file = 1; file <= numberOfFiles; file++) {

        let filePath = directoryPath + "/" + file + "." + "json"

        fs.unlink(filePath, (err) => {
            if (err) {
                console.error(err)
            } else {
                console.log(`${filePath} deleted successfully`)
                count++
                if (numberOfFiles == count) {
                    callback(null)
                }
            }
        })
    }
}

const createFiles = (directoryPath, numberOfFiles, callback) => {
    let count = 0
    for (let file = 1; file <= numberOfFiles; file++) {
        // console.log(file)
        
        let filePath = directoryPath + "/" + file + "." + "json"

        fs.writeFile(filePath, JSON.stringify({fileNumber : file}), (err) => {
            if (err) {
                console.error(err)
            } else {
                console.log(`${filePath} created successfully`)
                count++
                if (count == numberOfFiles) {
                    callback(null)
                }
            }
        })
    }
}

const fsProblem1 = (directoryPath, numberOfFiles) => {
    fs.mkdir(directoryPath, (err) => {
        if (err) {
            console.error(err)
        } else {
            console.log("Directory creation is successful")
            createFiles(directoryPath, numberOfFiles, (err) => {
                if (err) {
                    console.error(err)
                } else {
                    console.log('Files created successfully')
                    deleteFiles(directoryPath, numberOfFiles, (err) => {
                        if (err) {
                            console.error(err)
                        } else {
                            console.log("Files deleted successfully")
                            fs.rmdir(directoryPath, (err) => {
                                if (err) {
                                    console.error(err)
                                } else {
                                    console.log("Directory deletion is successful")
                                }
                            })
                        }
                    })
                }
            })
        }
    })
}

module.exports = fsProblem1
